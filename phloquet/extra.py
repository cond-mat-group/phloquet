#!/usr/bin/env python
############################################################################################
##### This module implements symmetry transformations.
############################################################################################

####################################
###########  IMPORTS   #############
####################################
import numpy as np
from scipy.integrate import quad, dblquad
import scipy.linalg as spla
import matplotlib.pyplot as plt
import pandas as pd
####################################




################################################################################################################################################
def search_2site_Floquet(t1_arr, mu11_arr, mu12_arr, t2_arr, mu21_arr, mu22_arr, alpha_arr, T_arr):


    print(f"t1 values: {t1_arr}")
    print(f"mu11 values: {mu11_arr}")
    print(f"mu12 values: {mu12_arr}")
    print(f"t2 values: {t2_arr}")
    print(f"mu21 values: {mu21_arr}")
    print(f"mu22 values: {mu22_arr}")
    print(f"alpha values: {alpha_arr}")
    print(f"T values: {T_arr}")

    thresh = 10**(-6)

    good_parameters = []
    for t1 in t1_arr:
        for mu11_idx,mu11 in enumerate(mu11_arr):
            for mu12_idx,mu12 in enumerate(mu12_arr):
                print_flag=True
                for t2 in t2_arr:
                    for mu21 in mu21_arr:
                        for mu22 in mu22_arr:
                            for alpha in alpha_arr:
                                for T in T_arr:
    
                                    if int(((mu11_idx+1)*(mu12_idx+1)/(len(mu11_arr)*len(mu12_arr))*100))%2==0 and print_flag==True:
                                        print_flag=False
                                        print(f"Calculating t1={t1}, mu11={mu11}, mu12={mu12}, t2={t2}, mu21={mu21}, mu22={mu22}, alpha={alpha}, T={T}")
    

                                    score = 0
                                    Heff = build_Heff_2site_Floquet(t1,mu11,mu12,t2,mu21,mu22,alpha,T)
                                    if np.abs(np.imag(Heff[0,1])) < thresh and np.abs(np.imag(Heff[1,0])) < thresh:
                                        score += 1
                                    if np.abs(np.imag(Heff[0,0])) < thresh:
                                        score += 1
                                    if np.abs(np.imag(Heff[1,1])) < thresh:
                                        score += 1
                                    if score>1:
                                        print("!!!!!!!!!!!!!!!!")
                                        print("Good parameters!")
                                        print("!!!!!!!!!!!!!!!!")

                                        print(f"The points with t1={t1}, mu11={mu11}, mu12={mu12}, t2={t2}, mu21={mu21}, mu22={mu22}, alpha={alpha}, T={T} has a score of {score}")
                                        good_parameters.append({'t1': t1, 
                                                                'mu11': mu11, 
                                                                'mu12': mu12,
                                                                't2': t2, 
                                                                'mu21': mu21, 
                                                                'mu22': mu22,
                                                                'alpha': alpha, 
                                                                'T': T,
                                                                'score': score})
    
    df = pd.DataFrame(good_parameters)
    df.to_csv("Good_parameters_2site_Floquet.csv")

    return good_parameters

################################################################################################################################################

################################################################################################################################################
def build_Heff_2site_Floquet(t1,mu11,mu12,t2,mu21,mu22,alpha,T):

    #build matrix:
    H1 = np.array([[1j*mu11,t1],[t1,1j*mu12]])
    H2 = np.array([[1j*mu21,t2],[t2,1j*mu22]])
    Heff = 1j/T*spla.logm(spla.expm(-1j*H1*(1-alpha)*T)@spla.expm(-1j*H2*alpha*T))

    return Heff
################################################################################################################################################




###################################
###################################
#####        PLOTTERS         #####
###################################
###################################
################################################################################################################################################


################################################################################################################################################



###################################
###################################
#####          UTILS          #####
###################################
###################################
def derivative(f, x):
    h = 1e-6  # Small step size for numerical differentiation
    return (f(x + h) - f(x)) / h
###################################



###################################
def myfunc(x):
    return x**2
###################################

