#!/usr/bin/env python
############################################################################################
##### This file calculates the eigenvalues of a given matrix (Hamiltonian).
############################################################################################

####################################
###########  IMPORTS   #############
####################################
from __future__ import division
from __future__ import print_function
import scipy as sp
import numpy as np
from . import utils

# Custom modules:
from . import input as inp
####################################


########################################################################
def diagonalize(Ham,
                input, 
                **kwargs):
    """
    Compute the eigenvalues and eigenvectors of the given Hamiltonian.
    
    Parameters:
    -----------
    Ham, instance of Hamiltonian class: the (possibly sparse) Hamiltonian.
    input, instance of Input class: the input information.

    **kwargs:
    - n_evals, int: the number of lowest eigenvalues to be computed.


    Returns
    -------
    evals, list of floats: the n_evals smallest eigenvalues.


    References
    ----------

    Notes
    -----

    Examples
    --------

    """

    if utils.is_sparse(Ham):
        H = Ham.todense()
    else:
        H = Ham
    
    # Calculate all eigenvalues:
    evals, evecs = sp.linalg.eig(H)
    return evals, evecs

########################################################################


