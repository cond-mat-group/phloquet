#!/usr/bin/env python
##########################################################################################
# This python program performs exact diagonalization of closed systems.
###########################################################################################

####################################
###########  IMPORTS   #############
####################################
from __future__ import division
from __future__ import print_function
import numpy as np
import time

# CUSTOM modules:
from . import input as inp
from . import glob 
from . import hams
from . import diag
from . import obs
from . import utils
from . import dynamics as dyn
from . import viz
from . import input_output as ino
####################################




############################################################################################################
def main(input_filename):

      # Get the input
      input = inp.Input(input_filename=input_filename)

      # print("input_filename:", input_filename)
      # print("stats:", input.stats)
      # print("model:", input.model)
      # print("sites:", input.sites)
      # print("PBC:", input.PBC)
      # print("sparse:", input.sparse)
      # print("Ham_par1:", input.Ham_par1)
      # print("Ham_par2:", input.Ham_par2)
      # print("Ham_par3:", input.Ham_par3)
      # print("Ham_par4:", input.Ham_par4)
      # print("Ham_par5:", input.Ham_par5)
      # print("Ham_par6:", input.Ham_par6)
      # print("Ham_par7:", input.Ham_par7)
      # print("Ham_par8:", input.Ham_par8)
      # print("Ham_par9:", input.Ham_par9)
      # print("Ham_par10:", input.Ham_par10)
      # print("Ham_par10:", input.Ham_par10)
      # print("Ham_par10:", input.Ham_par10)
      # print("n_lowest_eigenvalues:", input.n_lowest_eigenvalues)
      # print("maxiter_Arnoldi:", input.maxiter_Arnoldi)
      # print("save_evals:", input.save_evals)
      # print("compute_evecs:", input.compute_evecs)
      # print("save_evecs:", input.save_evecs)
      # print("load_input_parameters:", input.load_input_parameters)
      # print("dynamics:", input.dynamics)
      # print("initial_state:", input.initial_state)
      # print("final_time:", input.final_time)
      # print("dt:", input.dt)
      # print("print_gs_energy:", input.print_gs_energy)
      # print("print_es_energies:", input.print_es_energies)
      # print("print_gs:", input.print_gs)
      # print("print_es:", input.print_es)
      # print("dt:", input.dt)
      # print("plot_Hamiltonian:", input.plot_Hamiltonian)
      # print("plot_evals:", input.plot_evals)
      # print("plot_evecs:", input.plot_evecs)
      # print("plot_dynamics:", input.plot_dynamics)
      # print("movie_dynamics:", input.movie_dynamics)
      # print("verbose:", input.verbose)


      ####################################


      ################################################################
      ######### INPUT SELF-CONSISTENCY CHECKS
      ################################################################

      


      ################################################################
      ######### SETTING UP HAMILTONIAN
      ################################################################
      print("Setting up Hamiltonian...")
      ti = time.time()
      Ham = hams.Hamiltonian(input=input)
      tf = time.time()
      print(f"Time needed to set up Hamiltonian: {utils.get_time_diff(ti,tf)}")
      print("\n") 
      print("====================================")
      print(f"Model: {Ham.model}")   
      print("====================================")
      print("\n") 
      print("====================================")
      print("Parameters of the Hamiltonian:")
      print("Sites:", Ham.sites)
      print("Periodic boundary conditions?", Ham.PBC)
      # Ham_par1:
      if Ham.model=="Creutz-2step":
            print("Intracell hopping t0:", Ham.pars["t0"])
      # Ham_par2:
      if Ham.model=="Creutz-2step":
            print("Intercell hopping t1AB:", Ham.pars["t1AB"])
      # Ham_par3:
      if Ham.model=="Creutz-2step":
            print("Intercell hopping t1BA:", Ham.pars["t1BA"])
      # Ham_par4:
      if Ham.model=="Creutz-2step":
            print("Intercell hopping t2A:", Ham.pars["t2A"])
      # Ham_par5:
      if Ham.model=="Creutz-2step":
            print("Intercell hopping t2B:", Ham.pars["t2B"])
      # Ham_par6:
      if Ham.model=="Creutz-2step":
            print("Chemical potential on sublattice A:", "1j"+str(Ham.pars["muA"]))
       # Ham_par7:
      if Ham.model=="Creutz-2step":
            print("Chemical potential on sublattice B:", "1j"+str(Ham.pars["muB"]))
      # Ham_par8:
      if Ham.model=="Creutz-2step":
            print("Driving period T:", Ham.pars["T"])
      # Ham_par9:
      if Ham.model=="Creutz-2step":
            print("Driving step length alpha:", Ham.pars["alpha"])     

      print("====================================")
      print("\n") 

      # Visualize Hamiltonian matrix:
      if input.plot_Hamiltonian:
      
            for step,H in enumerate(Ham.Hams):
                  viz.visualize_hamiltonian(Ham=H, title=f"Hamiltonian at step {step}")
            viz.visualize_hamiltonian(Ham=Ham.U, title=f"Floquet operator")
            viz.visualize_hamiltonian(Ham=Ham.Heff, title=f"effective Floquet Hamiltonian")
            viz.visualize_hamiltonian(Ham=np.abs(Ham.Heff), title=f"magnitude of effective Floquet Hamiltonian")

      else: 
            print("====================================")
            print("Hamiltonian visualization skipped.")
            print("====================================")
      print("\n") 
      ################################################################


      ################################################################
      ######### DIAGONALIZATION
      ################################################################      
      print("====================================")
      print("Performing diagonalization...") 
      print("====================================")
      ti = time.time()
      evals_U, Floquet_evecs = diag.diagonalize(Ham=Ham.U,
                                                        input=input)
      quasienergies_U = 1j*np.log(evals_U)/Ham.pars["T"]

      #quasienergies_Heff, eff_Ham_evecs = diag.diagonalize(Ham=Ham.Heff,
      #                                                     input=input) 

      tf = time.time()
      print(f"Time needed to perform diagonalization: {utils.get_time_diff(ti,tf)}")
      # Saving eigenvalues:
      if input.save_evals == True:
            print("Saving eigenvalues (quasienergies)...")
            ino.save_eigenvalues(model=Ham.model,
                                 sites=Ham.sites,
                                 pars=Ham.pars, 
                                 BC=Ham.PBC,
                                 eigs=quasienergies_U)
      else: 
            print("Eigenvalues NOT saved.")
      # Saving eigenvectors:
      if input.save_evecs == True:
            print("Saving Floquet eigenstates...")
            ino.save_eigenvectors(model=Ham.model, 
                                  sites=Ham.sites, 
                                  pars=Ham.pars, 
                                  BC=Ham.PBC, 
                                  evecs=Floquet_evecs)
      
      else: 
            print("Eigenvectors NOT saved.")
      
      if input.plot_evals==True:
            viz.visualize_quasienergies(evals_U=evals_U,
                                        Ham=Ham)
      else:
            print("Quasienergy visualization skipped.")
      if input.plot_evecs==True:
            for j in range(np.shape(Floquet_evecs)[1]):
                  viz.visualize_eigenstate(evec=Floquet_evecs[:,j], 
                                           index=j,
                                           Ham=Ham)
      else:
            print("Eigenvector visualization skipped.")
      print("====================================")
      print("\n")   


      # Print some info:
      print("====================================")
      if input.print_quasienergies==True:
            print("Quasienergies:", quasienergies_U)

      else:
            print("Quasienergy output skipped.")
      
      if input.print_Floquet_states==True:
            print("Floquet eigenstates:")
            for k in range(1,len(Floquet_evecs[0,:])):
                  print(Floquet_evecs[:,k])
      else:
            print("Floquet eigenstate output skipped.")
      print("====================================")
      print("\n")   
      ################################################################


      # ################################################################
      # ######### GROUND-STATE OBSERVABLES
      # ################################################################
      # print("====================================")
      # print("Basis:")
      # print("====================================")
      # if Ham.stats=="spinful-fermions":
      #       print("Spin up:")
      #       print(Ham.basis_states_up)
      #       print("Spin down:")
      #       print(Ham.basis_states_down)
      # else:
      #       for b in Ham.basis_states:
      #             print(np.binary_repr(b,Ham.sites), b)
      # print("====================================")
      # print("\n")

      # print("====================================")
      # print("Calculating ground-state observables...")
      # print("====================================")
      # if input.IPR==True:
      #       print("Calculating IPR...")
      #       ti = time.time()
      #       IPR = obs.average_IPR(Ham=Ham, evecs=eigenvectors, verbose=input.verbose)
      #       tf = time.time()
      #       print(f"Time needed to calculate IPR: {utils.get_time_diff(ti,tf)}")
      #       print("IPR value:", IPR)
      #       print("Saving IPR...")
      #       ino.save_obs(model=Ham.model,
      #                    sites=Ham.sites,
      #                    pars=Ham.pars, 
      #                    BC=Ham.PBC,
      #                    obs_name="IPR",
      #                    obs_value=IPR)
      # print("====================================")
      # print("\n")

      # ################################################################



      # ################################################################
      # ######### DYNAMICS
      # ################################################################
      # if input.dynamics == True:
      #       print("====================================")
      #       print("Calculating time evolution...")
      #       print("====================================")
      #       ti = time.time()
      #       time_evolved_state = dyn.full_time_evolution(Ham=Ham, 
      #                                                    evals=eigenvalues, 
      #                                                    evecs=eigenvectors, 
      #                                                    initial_state=input.initial_state, 
      #                                                    final_time=input.final_time, 
      #                                                    dt=input.dt)
      #       tf = time.time()
      #       print(f"Time needed to calculate time evolution: {utils.get_time_diff(ti,tf)}")

      #       if input.plot_dynamics == True:
      #             print("Generating plots of time evolution...")
      #             if Ham.stats=="spinful-fermions":
      #                   raise NotImplementedError()
      #             else:
      #                   viz.plot_time_evolution_spinless(time_evolved_state=time_evolved_state, 
      #                                                    Ham=Ham,
      #                                                    initial_state=input.initial_state,
      #                                                    final_time=input.final_time,
      #                                                    dt=input.dt)
      #                   viz.plot_density_vs_t_spinless(time_evolved_state=time_evolved_state, 
      #                                                  Ham=Ham, 
      #                                                  initial_state=input.initial_state, 
      #                                                  dt=input.dt, 
      #                                                  final_time=input.final_time)
      #       else:             
      #             print(f"Plot of dynamics skipped.")


      #       if input.movie_dynamics == True:
      #             print("Generating movie for time evolution...")
      #             if Ham.stats=="spinful-fermions":
      #                   raise NotImplementedError()
      #             else:
      #                   viz.movie_time_evolution_spinless(time_evolved_state=time_evolved_state,
      #                                                     Ham=Ham,
      #                                                     initial_state=input.initial_state,
      #                                                     final_time=input.final_time,
      #                                                     dt=input.dt)
      #       else:             
      #             print(f"Movie for dynamics skipped.")

      #       print("====================================")
      #       print("\n")  

      # ################################################################


      return 
############################################################################################################





############################################################################################################
if __name__=="__main__":
      Ham = main("input.dat")
############################################################################################################