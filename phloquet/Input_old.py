import numpy as np

# Parameters of the Hamiltonian
num_rows = 11                                                 # Number of rows
num_cols = 11                                                 # Number of columns
hopping = 1                                                   # Hopping strength
initial_site = int((num_rows-1)/2*num_cols+(num_cols-1)/2)    # Index where the initial state is located

# Parameters for the integration
t_i = 0.0                                                     # Initial time
t_f = 6.0                                                     # Final time
dt = 0.001                                                    # Step size


# Parameters for the plots
plot_times = [0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0]              # Times at which to plot the density
dens_vmin = 0.0                                               # Minimal values of the density to plot
dens_vmax = 0.1                                               # Maximal values of the density to plot

# Other:
verbose = False