import numpy as np
import matplotlib as m
import matplotlib.pyplot as plt

# Custom made modules:
import Input as inp

##################
##################
### ASSEMBLERS ###
##################
##################

#############################################################################################
def tight_binding_model_2D_OBC(num_rows: int,
                               num_cols: int, 
                               t: float,
                               ):
    """
    Constructs the 2D tight binding model with open boundary conditions.

    Parameters:
    ----------
    num_rows, int: number of rows in the 2D geometry.
    
    num_cols, int: number of columns in the 2D geometry.
    
    t, float: hopping strength.

    Returns:
    --------
    H, (num_rows*num_cols, num_rows*num_cols) numpy array: the Hamiltonian matrix.


    """

    # Calculate the total number of sites
    num_sites = num_rows * num_cols
    
    # Create an empty Hamiltonian matrix
    H = np.zeros((num_sites, num_sites))
    
    # Fill the off-diagonal elements with the hopping parameter t
    for i in range(num_rows):
        for j in range(num_cols):
            site_index = get_site_index_from_coord(i, j, inp.num_cols)
            
            if i > 0:
                H[site_index, site_index - num_cols] = -t
            if i < num_rows - 1:
                H[site_index, site_index + num_cols] = -t
            if j > 0:
                H[site_index, site_index - 1] = -t
            if j < num_cols - 1:
                H[site_index, site_index + 1] = -t
    
    return H

#############################################################################################



#############################################################################################
def get_in_state(num_rows: int,
                 num_cols: int,
                 initial_site: int,
                 ): 
    """
    Creates an initial state.

    Parameters:
    ----------
    num_rows, int: number of rows in the 2D geometry.
    
    num_cols, int: number of columns in the 2D geometry.
    
    t, float: hopping strength.

    Returns:
    --------
    H, (num_rows*num_cols, num_rows*num_cols) numpy array: the Hamiltonian matrix.

    """

    # Populate the initial state at the given site:
    in_state = np.zeros([num_rows*num_cols],dtype=complex)
    in_state[initial_site]=1

    return in_state
#############################################################################################




#################
#################
### COMPUTERS ###
#################
#############################################################################################

#############################################################################################
def RK4_step(y: np.array,
             t: float,
             dt: float):
    """
    Performs a 4-th order Runge-Kutta integration step.

    Parameters:
    ----------
    y, numpy array: the current value of the wave function (state).
    t, float: the current time.
    dt, float: the value of the integration time step.

    Returns:
    --------
    step, numpy array: the value of the state after the RK4 step.
    
    """
    def G(y,t):
        # Constructs the right-hand side of the time-dependent Schrödinger equation
        # Note that t here is irrelevant for time-independent Hamiltonians!
        return -1j*np.dot(Ham,y)

    k1 = G(y,t)
    k2 = G(y + 0.5*k1*dt, t + 0.5*dt)
    k3 = G(y + 0.5*k2*dt, t + 0.5*dt)
    k4 = G(y + k3*dt, t + dt)

    step = dt*(k1+2*k2+2*k3+k4)/6

    return step
#############################################################################################

#############################################################################################
def time_evolve(y0: np.array,
                t_i: float = 0.0, 
                t_f: float = 10.0,
                dt: float = 0.001,
                ):
    """
    Time evolves the Schrödinger equation from t_i to t_f in steps of dt.
    
    Parameters:
    ----------
    y0, numpy array: the initial state.
    t_i, float: the initial time in the evolution.
    t_f, float: the final time in the evolution.
    dt, float: the value of the integration time step.

    Returns:
    --------
    state, numpy array of shape (times x sites): encodes the full time evolution at every site.
    
    """

    # Integration times:
    times = np.arange(t_i, t_f, dt)

    # Total number of time points:
    t_len = len(times)+1

    # Matrix that will contain the evolution of the state:
    state = np.zeros([t_len,inp.num_rows*inp.num_cols], dtype=complex) #each row is the state in the different times, 1st row t=0, 2nd row t=dt etc.

    # Time integration via RK4:
    tt = 0
    y = y0
    state[tt,:] = y0
    for t in times:
        y = y + RK4_step(y,np.round(t,4),dt)
        tt += 1
        state[tt,:] = y
    
    return state 
#############################################################################################



##################
##################
###  PLOTTERS  ###
##################
#############################################################################################
def plot_prob(state: np.array,
              time_array: np.array,
              plot_times = list,
              vmin: float = 0,
              vmax: float = 0.1
              ):
    """
    Plots the evolution of the probability density at every site for selected times.
    
    Parameters:
    ----------
    state, numpy array of shape (times x sites): encodes the full time evolution at every site.
    time_array, numpy array: all the times in the time evolution.
    plot_times, list: all the times that should be plotted.
    vmin, float: minimal value of the density in the plot.
    vmax, float: maximal value of the density in the plot.

    Returns:
    --------
    state, numpy array of shape (times x sites): encodes the full time evolution at every site.
    
    """

    for t in plot_times:

        # Get index of the current time in the full array of all the times:
        time_idx = get_time_idx(time_array=time_array, time=t)
        # Reshape the vectorized density into the original 2D configuration:
        state_2d = np.reshape(np.abs(state[time_idx,:])**2,([inp.num_cols,inp.num_rows]))

        # Plotting part:
        plt.clf()
        
        plot = plt.imshow(state_2d, vmin=vmin, vmax=vmax)
        cbar = plt.colorbar(plot)
        plt.title('t='+str(np.round(t*inp.dt,2)))
        plt.xlabel(r"Site $x_i$")
        plt.ylabel(r"Site $y_i$")
        plt.savefig('4evolution_t_' + str(np.round(t,2)) + '.png', dpi=200, bbox_inches='tight')
        plt.close()

    return
#############################################################################################





###################
###################
### SUBROUTINES ###
###################
#############################################################################################
def get_site_index_from_coord(idx_x: int, 
                              idx_y: int,
                              num_cols: int,
                              ):
    """
    Transforms 2D coordinates into one-dimensional site index.

    Parameters:
    ----------
    idx_x, int: coordinate index in the x direction.

    idx_y, int: coordinate index in the y direction.

    num_cols, int: number of columns in the 2D geometry.

    
    Returns:
    --------
    site_index, int: the index in the vectorized representation.

    """
    site_index = idx_x * num_cols + idx_y
    
    return site_index
#############################################################################################

#############################################################################################
def get_time_idx(time_array: np.array,
                 time: float):
    """
    Selects the index in the time array that is closest to the target time.

    Parameters:
    ----------
    time_array, numpy array: the array containing all the discretized times in the evolution.

    time, float: the target time whose index should be extracted.

    
    Returns:
    --------
    time_index, int: the index in the time array that is closest to the target time.

    """
    site_index = min(range(len(time_array)), key = lambda i: abs(time_array[i]-time))
    closest_time = time_array[site_index]

    if np.abs(closest_time - time) > 1e-12:
        print(f"There is no exact match with the requested time {time}. The closest value found was {closest_time} at index {site_index}.")

    return site_index
#############################################################################################


#############################################################################################
def test_inputs():
    print("\n======================")
    print("Hamiltonian parameters:")
    print("======================")
    print(f"Number of rows: {inp.num_rows}")
    print(f"Number of columns: {inp.num_cols}")
    print(f"Initial state site: {inp.initial_site}")
    print("\n")

    print("\n======================")
    print("Integration parameters:")
    print("=======================")
    print(f"Integration initial time: {inp.t_i}")
    print(f"Integration final time: {inp.t_f}")
    print(f"Integration time step: {inp.dt}")
    print(f"\n")

    return 
#############################################################################################

#############################################################################################
def test_Ham(Ham):

    # Plotting part:
    plt.imshow(Ham)
    plt.title("Hamiltonian matrix")
    plt.xlabel(r"Site $x_i$")
    plt.ylabel(r"Site $y_i$")
    plt.show()
    plt.close()

    return 
#############################################################################################

#############################################################################################
def test_initial_state(y):

    state_2d = np.reshape(y,([inp.num_cols,inp.num_rows]))

    # Plotting part:
    fig = plt.figure()
    fig.suptitle("Initial state")
    ax1 = fig.add_subplot(121)
    ax1.imshow(np.real(state_2d))
    ax1.set_xlabel(r"Site index $x_i$")
    ax1.set_ylabel(r"Site index $y_i$")
    ax1.set_title("Real part")

    ax2 = fig.add_subplot(122)
    ax2.imshow(np.imag(state_2d))
    ax2.set_xlabel(r"Site index $x_i$")
    ax2.set_ylabel(r"Site index $y_i$")
    ax2.set_title("Imaginary part")

    plt.tight_layout()
    plt.show()
    plt.close()

    return 
#############################################################################################

#############################################################################################
if __name__ == "__main__":

    # Input tests:
    if inp.verbose==True:
        test_inputs()

    # Building quantities:
    Ham = tight_binding_model_2D_OBC(inp.num_rows, inp.num_cols, inp.hopping)
    if inp.verbose==True:
        test_Ham(Ham)
    y0 = get_in_state(inp.num_rows, inp.num_cols, inp.initial_site)
    if inp.verbose==True:
        test_initial_state(y0)
    time_array = np.arange(inp.t_i, inp.t_f, inp.dt)

    # Integration
    state = time_evolve(y0=y0, t_i=inp.t_i, t_f=inp.t_f, dt=inp.dt)

    # Plots
    plot_prob(state = state, time_array = time_array, plot_times = inp.plot_times, vmin=inp.dens_vmin, vmax=inp.dens_vmax)

#############################################################################################




##########
# UNUSED #
##########
#############################################################################################

    # for t in range(0,len(tim),int(len(tim)/10)):
    #     print(t)
    #     plt.clf()
    #     plt.imshow(np.reshape(np.abs(prob[t,:])**2,([num_cols,num_rows])),vmin=0,vmax=1)
    #     plt.title('t='+str(np.round(t*dt,2)))
    #     plt.colorbar()
    #     plt.savefig('1evolution_t_'+str(t)+'.png', dpi=200,bbox_inches='tight')

    # for t in range(0,len(tim),int(len(tim)/10)):
    #     print(t)
    #     plt.clf()
    #     plt.imshow(np.reshape(np.abs(prob[t,:])**2,([num_cols,num_rows])),vmin=0,vmax=0.5)
    #     plt.title('t='+str(np.round(t*dt,2)))
    #     plt.colorbar()
    #     plt.savefig('2evolution_t_'+str(t)+'.png', dpi=200,bbox_inches='tight')