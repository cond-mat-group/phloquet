#!/usr/bin/env python

####################################
###########  IMPORTS   #############
####################################
from __future__ import division
from __future__ import print_function
import numpy as np
import scipy as sp

# Custom modules:
from . import input as inp
from . import utils
####################################


######################################################################################
class Hamiltonian:
    """
    Methods
    ----------
     

    Parameters
    ----------
    

    Examples
    --------
  
    """
  
  
    def __init__(self, 
                 input,
                 **kwargs):
        
        """
        Parameters:
        ----------
        input, instance of Input class

        """
        
        # Get parameters:
        self.dim = input.dim
        self.model = input.model
        self.sites = input.sites
        self.PBC = input.PBC
        self.get_parameters(input)

        # Perform checks:
        self.input_checks(input)
    
        # Assemble operators:
        self.Hams, self.U = self.load_Floquet_operator(input)
        self.Heff = self.get_eff_Floquet_Ham(input)

        #########################################

    ######################################################################################

    ######################################################################################
    def input_checks(self,
                     input):
 
        if self.model=="Creutz-2step" and self.dim!=1:
            raise NotImplementedError(f"Please choose dimension equal 1 for model {self.model}")

        return

    ######################################################################################

    ######################################################################################
    def load_Floquet_operator(self,
                              input):
        """
        Creates and stores the chosen Floquet operator.

        Parameters:
        -----------
        All through self and input.

        Returns:
        --------
        U, numpy array: the Floquet operator.

        """

        ######################################################################################################################################
        if self.model=="Creutz-2step":
            # Basis is 2N in length: (1A,1B,2A,2B, ..., NA,NB)
            
            # Assemble Hamiltonian for each step:
            Hams = []
            for step in [0,1]:
                
                # Empty lists for sparse Hamiltonian
                rows = []
                cols = []
                mat_els = []
    
                # Tunneling elements:
                bonds_intra = self.get_bonds(2*self.sites,1)    
                del bonds_intra[1::2]         # intracell bonds (A->B)
                bonds_interBA = self.get_bonds(2*self.sites,3)
                del bonds_interBA[1::2]         # nn intracell bonds (B->A)
                bonds_interAB = self.get_bonds(2*self.sites,1)
                del bonds_interAB[0::2]         # nn intracell bonds (A->B)
                bonds_interAA = self.get_bonds(2*self.sites,2)
                del bonds_interAA[1::2]         # nn intracell bonds (A->A)
                bonds_interBB = self.get_bonds(2*self.sites,2)
                del bonds_interBB[0::2]         # nn intracell bonds (A->B)
            
                # print("bonds_intra:",bonds_intra)
                # print("bonds_interAB:",bonds_interAB)
                # print("bonds_interBA:",bonds_interBA)
                # print("bonds_interAA:",bonds_interAA)
                # print("bonds_interBB:",bonds_interBB)
    
                for b in bonds_intra:   
                    rows.append(b[0])
                    cols.append(b[1])
                    mat_els.append(self.pars["t0"][step])
                    rows.append(b[1])
                    cols.append(b[0])
                    mat_els.append(self.pars["t0"][step])

                for b in bonds_interBA:   
                    rows.append(b[0])
                    cols.append(b[1])
                    mat_els.append(self.pars["t1BA"][step])
                    rows.append(b[1])
                    cols.append(b[0])
                    mat_els.append(self.pars["t1BA"][step])
        
                for b in bonds_interAB:   
                    rows.append(b[0])
                    cols.append(b[1])
                    mat_els.append(self.pars["t1AB"][step])
                    rows.append(b[1])
                    cols.append(b[0])
                    mat_els.append(self.pars["t1AB"][step])
        
                for b in bonds_interAA:   
                    rows.append(b[0])
                    cols.append(b[1])
                    mat_els.append(self.pars["t2A"][step])
                    rows.append(b[1])
                    cols.append(b[0])
                    mat_els.append(self.pars["t2A"][step])
        
                for b in bonds_interBB:   
                    rows.append(b[0])
                    cols.append(b[1])
                    mat_els.append(self.pars["t2B"][step])
                    rows.append(b[1])
                    cols.append(b[0])
                    mat_els.append(self.pars["t2B"][step])

                # Chemical potential:
                for i in range(2*self.sites):
                    rows.append(i)
                    cols.append(i)
                    if i%2==0:
                        mat_els.append(1j*self.pars["muA"][step])   
                    elif i%2==1:
                        mat_els.append(1j*self.pars["muB"][step])   

                # Assemble Hamiltonians as matrices in CSR format
                sparse_matrix = sp.sparse.csr_matrix((mat_els, (rows, cols)), shape=(2*self.sites, 2*self.sites))

                Hams.append(sparse_matrix)
    
                if input.verbose == True:
                    print(f"Step {step}:")
                    print("rows:", rows)
                    print("cols:", cols)
                    print("matrix elements:", mat_els)
                    print(f"Hamiltonian for step {step}:")
                    print(sparse_matrix.todense())
            
            # Exponentiate the sparse matrix
            U = np.dot(sp.linalg.expm(-1j*Hams[1].todense()*self.pars["T"]*(1 - self.pars["alpha"])),sp.linalg.expm(-1j*Hams[0].todense()*self.pars["T"]*self.pars["alpha"]))
            
            if input.verbose == True:
                print(f"Floquet operator:")
                print(U)
    
            return Hams,U

        ######################################################################################################################################

    ######################################################################################
    def get_eff_Floquet_Ham(self,input):
        """
        Creates and stores the matrix elements of the chosen Floquet operator.

        Parameters:
        -----------
        All through self

        Returns:
        --------
        Heff, numpy array: the effective Floquet Hamiltonian.

        """

        if self.model=="Creutz-2step":
    
            Heff = 1j/self.pars["T"]*sp.linalg.logm(self.U)

        if input.verbose == True:
            print(f"Effective Floquet Hamiltonian:")
            print(Heff)
    
        return Heff

    ######################################################################################



    ######################################################################################
    def get_bonds(self, sites, order):
        """
        Returns a list of sites connected by bonds of a given order.

        Notes:
        ------
        For multiband models, e.g. the Creutz ladder or SSH model, 
        you need to account for the sublattice/band structure when
        calling this function.
        E.g. get_bonds(1) gives bonds {(1,A)->(1,B), (1,B)->(2,A), (2,A)->(2,B), etc.}        

        Examples:
        --------
        get_bonds(1) --> nearest-neighbor bonds
        get_bonds(2) --> next-nearest-neighbor bonds

        """
        # Define chain lattice
        if self.PBC:
            bonds = [(site, (site+order)%sites) for site in range(sites)]
        else:
            bonds = [(site, site+order) for site in range(sites-order)]

        return bonds
    ######################################################################################



    ######################################################################################
    def get_parameters(self,
                       input):
        
        if self.model == "Creutz-2step":
            t0 = input.Ham_par1
            t1AB = input.Ham_par2
            t1BA = input.Ham_par3
            t2A = input.Ham_par4
            t2B = input.Ham_par5
            muA = input.Ham_par6
            muB = input.Ham_par7
            T = input.Ham_par8
            alpha = input.Ham_par9
    
            pars = {"t0": t0,
                    "t1AB": t1AB,
                    "t1BA": t1BA,
                    "t2A": t2A,
                    "t2B": t2B,
                    "muA": muA,
                    "muB": muB,
                    "T": T,
                    "alpha": alpha,
                    }
   
        # Sort dictionary:  (needed for saving/loading files properly)
        par_keys = list(pars.keys())
        par_keys.sort()
        sorted_pars = {i: pars[i] for i in par_keys}

        self.pars = sorted_pars

        return

    ######################################################################################