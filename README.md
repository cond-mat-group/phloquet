# PHLOQUET

## Description

PHLOQUET is a collection of python modules used to calculate the spectral and dynamical properties of Non-Hermitian Floquet Hamiltonians arising in the context of photonic systems. 
Currently, PHLOQUET supports the exact diagonalization of the following models:
- 2-step Creutz ladder


## Installation
TBC

## Usage
To use PHLOQUET:
1) Copy the template input file:
python3 -m phloquet.initialize.py
2) Configure the input file (input.dat) with the parameters of your system.
3) Execute PHLOQUET:
python3 -m phloquet.run.py

## Support
For support, please contact moligninip@gmail.com.

## Roadmap
Short term goals:
- Python packaging.
- Simple GUI.

Medium term goals:
- 

Long term goals:
- 

## Contributing
If you would like to contribute to this project, please let me know!

## Authors and acknowledgment
Paolo Molignini (University of Stockholm)

## License
GPLv3 license.

## Project status
Ongoing
